package it.bevilacquamarmi.valuta.db

import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import android.content.Context
import it.bevilacquamarmi.valuta.db.dao.CurrencyDao
import it.bevilacquamarmi.valuta.db.entities.CurrencyInfo
import it.bevilacquamarmi.valuta.util.SingletonHolder

@Database(entities = [(CurrencyInfo::class)], version = 2)
abstract class AppDatabase protected constructor(): RoomDatabase() {

    abstract fun currency(): CurrencyDao

    companion object : SingletonHolder<AppDatabase, Context>({
        Room.databaseBuilder(it.applicationContext, AppDatabase::class.java, "currency_db")
                .addMigrations(Companion.MIGRATION_1_2)
                .build()
    }) {
        private val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("DROP TABLE currency")
            }
        }
    }
}