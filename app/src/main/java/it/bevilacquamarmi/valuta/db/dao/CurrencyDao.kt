package it.bevilacquamarmi.valuta.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import it.bevilacquamarmi.valuta.db.entities.CurrencyInfo
import it.bevilacquamarmi.valuta.db.typeconverters.GeoPositionConverter

@Dao
@TypeConverters(GeoPositionConverter::class)
interface CurrencyDao {

    @Query("SELECT * FROM currency ORDER BY favorite DESC, uid")
    fun getAll(): LiveData<List<CurrencyInfo>>

    @Query("SELECT * FROM currency ORDER BY favorite DESC, uid")
    fun getAllStatic(): List<CurrencyInfo>

    @Query("SELECT * FROM currency WHERE favorite = 1")
    fun getFavorites(): List<CurrencyInfo>

    @Query("SELECT * FROM currency WHERE geoPosition = :arg0 ORDER BY favorite DESC, uid")
    fun getByGeoPosition(arg0: Int): List<CurrencyInfo>

    @Query("SELECT * FROM currency WHERE uid IN (:arg0)")
    fun getById(vararg arg0: Long): List<CurrencyInfo>

    @Query("DELETE FROM currency")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg list: CurrencyInfo)
}