package it.bevilacquamarmi.valuta.db.entities

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import it.bevilacquamarmi.valuta.db.typeconverters.GeoPositionConverter
import it.bevilacquamarmi.valuta.util.GeoPosition
import it.bevilacquamarmi.valuta.util.extensions.toGeoPosition

@Entity(tableName = "currency", indices = [Index(value = ["uid"], unique = true)])
class CurrencyInfo : Parcelable {
    @PrimaryKey(autoGenerate = true)
    var uid: Long = -1

    @ColumnInfo(name = "name")
    var name: String = ""

    @ColumnInfo(name = "ratio")
    var ratio: Float = 1f

    @ColumnInfo(name = "favorite")
    var favorite: Int = 0

    @ColumnInfo(name = "geoPosition")
    @TypeConverters(GeoPositionConverter::class)
    var geoPosition: GeoPosition = GeoPosition.OTHER

    @Ignore
    constructor()

    @Ignore
    constructor(parcel: Parcel) {
        uid = parcel.readLong()
        name = parcel.readString() ?: ""
        ratio = parcel.readFloat()
        favorite = parcel.readInt()
        geoPosition = parcel.readInt().toGeoPosition()
    }

    constructor(uid: Long, name: String, ratio: Float, favorite: Int, geoPosition: GeoPosition) {
        this.uid = uid
        this.name = name
        this.ratio = ratio
        this.favorite = favorite
        this.geoPosition = geoPosition
    }

    override fun describeContents() = 0

    override fun writeToParcel(parcel: Parcel?, flags: Int) {
        if (parcel == null) {
            return
        }

        parcel.writeLong(uid)
        parcel.writeString(name)
        parcel.writeFloat(ratio)
        parcel.writeInt(favorite)
        parcel.writeInt(geoPosition.ordinal)
    }

    override fun equals(other: Any?): Boolean {
        if (other !is CurrencyInfo) {
            return false
        }

        return other.name == name && other.ratio == ratio && other.geoPosition == geoPosition
    }

    override fun hashCode() = super.hashCode() + 1

    fun getCopyText() = "${String.format("%.3f", ratio)} $name / EUR "

    companion object CREATOR : Parcelable.Creator<CurrencyInfo> {
        override fun createFromParcel(parcel: Parcel) = CurrencyInfo(parcel)
        override fun newArray(size: Int) = Array(size) { CurrencyInfo() }
    }
}