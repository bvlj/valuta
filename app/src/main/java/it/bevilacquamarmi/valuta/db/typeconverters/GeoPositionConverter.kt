package it.bevilacquamarmi.valuta.db.typeconverters

import androidx.room.TypeConverter
import it.bevilacquamarmi.valuta.util.GeoPosition
import it.bevilacquamarmi.valuta.util.extensions.toGeoPosition

class GeoPositionConverter {

    @TypeConverter
    fun toInt(value: GeoPosition?): Int? = value?.ordinal ?: 0

    @TypeConverter
    fun toGeoPosition(value: Int?): GeoPosition? = value?.toGeoPosition() ?: GeoPosition.OTHER
}