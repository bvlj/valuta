package it.bevilacquamarmi.valuta.db

import android.os.AsyncTask

abstract class DatabaseTask<I, O>(protected val db: AppDatabase) : AsyncTask<I, Int, O>()