package it.bevilacquamarmi.valuta.util.network

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Build
import android.preference.PreferenceManager
import android.util.Log
import androidx.annotation.IntDef
import androidx.lifecycle.LiveData
import it.bevilacquamarmi.valuta.util.extensions.get
import it.bevilacquamarmi.valuta.util.extensions.set

class LiveSyncStatus(private val context: Context) :
        LiveData<@LiveSyncStatus.Companion.SyncStatus Int>() {
    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    private val connectivityManager = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        context.getSystemService(ConnectivityManager::class.java)
    else
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val info = connectivityManager.activeNetworkInfo

            val isConnected = info != null && info.isConnected
            val isDataOld = shouldRefresh()

            var shouldSync = false

            if (isConnected) {
                if (isDataOld) {
                    shouldSync = true
                    postValue(SHOULD_UPDATE)
                } else {
                    postValue(UP_TO_DATE)
                }
            } else {
                shouldSync = isDataOld
                postValue(NO_CONNECTION)
            }

            prefs[KEY_SHOULD_SYNC] = shouldSync
        }
    }


    override fun onActive() {
        super.onActive()

        val intentFilter = IntentFilter()
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        intentFilter.addAction(BROADCAST_ACTION_SYNC)

        context.registerReceiver(receiver, intentFilter)
    }

    override fun onInactive() {
        context.unregisterReceiver(receiver)

        super.onInactive()
    }

    private fun shouldRefresh(): Boolean {
        val stored = prefs[KEY_LAST_SYNC, System.currentTimeMillis()]
        val current = System.currentTimeMillis()

        Log.d("OHAI", "${current - stored > MIN_DIFF}")
        return current - stored > MIN_DIFF
    }

    companion object {
        const val BROADCAST_ACTION_SYNC = "it.bevilacquamarmi.valuta.action.sync"

        const val KEY_LAST_SYNC = "status_sync_last"
        private const val KEY_SHOULD_SYNC = "status_sync_requested"

        private const val MIN_DIFF = 1000 * 60 * 60 * 24 // 1 day to millisec

        @IntDef(UP_TO_DATE, NO_CONNECTION, SHOULD_UPDATE)
        @Retention(AnnotationRetention.SOURCE)
        annotation class SyncStatus

        const val UP_TO_DATE = 0
        const val NO_CONNECTION = 1
        const val SHOULD_UPDATE = 2
    }
}