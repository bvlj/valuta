package it.bevilacquamarmi.valuta.util.extensions

import it.bevilacquamarmi.valuta.util.GeoPosition

fun Int.toGeoPosition() = when (this) {
    1 -> GeoPosition.AFRICA
    2 -> GeoPosition.ASIA
    3 -> GeoPosition.EUROPE
    4 -> GeoPosition.NORTH_AMERICA
    5 -> GeoPosition.OCEANIA
    6 -> GeoPosition.SOUTH_AMERICA
    else -> GeoPosition.OTHER
}