package it.bevilacquamarmi.valuta.util.extensions

import android.content.res.Resources
import androidx.annotation.DrawableRes
import it.bevilacquamarmi.valuta.R
import it.bevilacquamarmi.valuta.db.entities.CurrencyInfo
import it.bevilacquamarmi.valuta.util.CurrencyUtils

@DrawableRes
fun CurrencyInfo.getFlag(): Int = when (name) {
    CurrencyUtils.CurrencyEnum.AUD.name -> R.drawable.ic_currency_aud
    CurrencyUtils.CurrencyEnum.BGN.name -> R.drawable.ic_currency_bgn
    CurrencyUtils.CurrencyEnum.BRL.name -> R.drawable.ic_currency_brl
    CurrencyUtils.CurrencyEnum.CAD.name -> R.drawable.ic_currency_cad
    CurrencyUtils.CurrencyEnum.CHF.name -> R.drawable.ic_currency_chf
    CurrencyUtils.CurrencyEnum.CNY.name -> R.drawable.ic_currency_cny
    CurrencyUtils.CurrencyEnum.CZK.name -> R.drawable.ic_currency_czk
    CurrencyUtils.CurrencyEnum.DKK.name -> R.drawable.ic_currency_dkk
    CurrencyUtils.CurrencyEnum.EUR.name -> R.drawable.ic_currency_eur
    CurrencyUtils.CurrencyEnum.GBP.name -> R.drawable.ic_currency_gbp
    CurrencyUtils.CurrencyEnum.HKD.name -> R.drawable.ic_currency_hkd
    CurrencyUtils.CurrencyEnum.HRK.name -> R.drawable.ic_currency_hrk
    CurrencyUtils.CurrencyEnum.HUF.name -> R.drawable.ic_currency_huf
    CurrencyUtils.CurrencyEnum.IDR.name -> R.drawable.ic_currency_idr
    CurrencyUtils.CurrencyEnum.ILS.name -> R.drawable.ic_currency_ils
    CurrencyUtils.CurrencyEnum.INR.name -> R.drawable.ic_currency_inr
    CurrencyUtils.CurrencyEnum.ISK.name -> R.drawable.ic_currency_isk
    CurrencyUtils.CurrencyEnum.JPY.name -> R.drawable.ic_currency_jpy
    CurrencyUtils.CurrencyEnum.KRW.name -> R.drawable.ic_currency_krw
    CurrencyUtils.CurrencyEnum.MXN.name -> R.drawable.ic_currency_mxn
    CurrencyUtils.CurrencyEnum.MYR.name -> R.drawable.ic_currency_myr
    CurrencyUtils.CurrencyEnum.NOK.name -> R.drawable.ic_currency_nok
    CurrencyUtils.CurrencyEnum.NZD.name -> R.drawable.ic_currency_nzd
    CurrencyUtils.CurrencyEnum.PHP.name -> R.drawable.ic_currency_php
    CurrencyUtils.CurrencyEnum.PLN.name -> R.drawable.ic_currency_pln
    CurrencyUtils.CurrencyEnum.RON.name -> R.drawable.ic_currency_ron
    CurrencyUtils.CurrencyEnum.RUB.name -> R.drawable.ic_currency_rub
    CurrencyUtils.CurrencyEnum.SEK.name -> R.drawable.ic_currency_sek
    CurrencyUtils.CurrencyEnum.SGD.name -> R.drawable.ic_currency_sgd
    CurrencyUtils.CurrencyEnum.TRY.name -> R.drawable.ic_currency_try
    CurrencyUtils.CurrencyEnum.THB.name -> R.drawable.ic_currency_thb
    CurrencyUtils.CurrencyEnum.USD.name -> R.drawable.ic_currency_usd
    CurrencyUtils.CurrencyEnum.ZAR.name -> R.drawable.ic_currency_zar
    else -> R.drawable.ic_currency_unknown
}

fun CurrencyInfo.getSymbol(resources: Resources): String {
    val array = resources.getStringArray(R.array.currency_symbols)
    return array[uid.toInt()]
}
