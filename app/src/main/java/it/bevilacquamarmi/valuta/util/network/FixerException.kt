package it.bevilacquamarmi.valuta.util.network

import org.json.JSONObject

class FixerException(private val source: JSONObject) : Throwable() {

    override val message: String
        get() {
            val error = source.getJSONObject("error")
            val code = error.getInt("code")
            val info = error.getString("info")
            return "Error $code: $info"
        }
}