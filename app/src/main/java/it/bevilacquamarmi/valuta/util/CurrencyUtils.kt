package it.bevilacquamarmi.valuta.util

import android.content.res.Resources
import it.bevilacquamarmi.valuta.BuildConfig
import it.bevilacquamarmi.valuta.R
import it.bevilacquamarmi.valuta.util.extensions.toCurrencyEnum

object CurrencyUtils {
    private val HTTP = if (BuildConfig.DEBUG) "http" else "https"
    private val FIXER_HISTORY_API_URL = "$HTTP://data.fixer.io/api/%1\$s?" +
        "access_key=${BuildConfig.FIXER_API}&symbols=%2\$s"

    val DEFAULT_CURRENCY_UID = CurrencyEnum.EUR.ordinal.toLong()
    val FIXER_API_URL = "$HTTP://data.fixer.io/api/latest?" +
            "access_key=${BuildConfig.FIXER_API}&symbols=${BuildConfig.FIXER_SYMBOLS}"

    enum class CurrencyEnum {
        XXX, AUD, BGN, BRL, CAD,
        CHF, CNY, CZK, DKK, EUR,
        GBP, HKD, HRK, HUF, IDR,
        ILS, INR, ISK, JPY, KRW,
        MXN, MYR, NOK, NZD, PHP,
        PLN, RON, RUB, SEK, SGD,
        THB, TRY, USD, ZAR
    }


    fun getSymbol(resources: Resources, name: String): String {
        val id = name.toCurrencyEnum().ordinal
        val array = resources.getStringArray(R.array.currency_symbols)
        return array[id]
    }

    fun getHistoryAPIURL(date: String, name: String) =
            FIXER_HISTORY_API_URL.format(date, name)
}