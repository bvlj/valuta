package it.bevilacquamarmi.valuta.util

enum class GeoPosition {
    OTHER,
    AFRICA,
    ASIA,
    EUROPE,
    NORTH_AMERICA,
    OCEANIA,
    SOUTH_AMERICA
}