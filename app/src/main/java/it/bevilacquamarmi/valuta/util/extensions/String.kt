package it.bevilacquamarmi.valuta.util.extensions

import it.bevilacquamarmi.valuta.util.GeoPosition
import it.bevilacquamarmi.valuta.util.CurrencyUtils

fun String.toCurrencyEnum(): CurrencyUtils.CurrencyEnum = when (this) {
    CurrencyUtils.CurrencyEnum.AUD.name -> CurrencyUtils.CurrencyEnum.AUD
    CurrencyUtils.CurrencyEnum.BGN.name -> CurrencyUtils.CurrencyEnum.BGN
    CurrencyUtils.CurrencyEnum.BRL.name -> CurrencyUtils.CurrencyEnum.BRL
    CurrencyUtils.CurrencyEnum.CAD.name -> CurrencyUtils.CurrencyEnum.CAD
    CurrencyUtils.CurrencyEnum.CHF.name -> CurrencyUtils.CurrencyEnum.CHF
    CurrencyUtils.CurrencyEnum.CNY.name -> CurrencyUtils.CurrencyEnum.CNY
    CurrencyUtils.CurrencyEnum.CZK.name -> CurrencyUtils.CurrencyEnum.CZK
    CurrencyUtils.CurrencyEnum.DKK.name -> CurrencyUtils.CurrencyEnum.DKK
    CurrencyUtils.CurrencyEnum.EUR.name -> CurrencyUtils.CurrencyEnum.EUR
    CurrencyUtils.CurrencyEnum.GBP.name -> CurrencyUtils.CurrencyEnum.GBP
    CurrencyUtils.CurrencyEnum.HKD.name -> CurrencyUtils.CurrencyEnum.HKD
    CurrencyUtils.CurrencyEnum.HRK.name -> CurrencyUtils.CurrencyEnum.HRK
    CurrencyUtils.CurrencyEnum.HUF.name -> CurrencyUtils.CurrencyEnum.HUF
    CurrencyUtils.CurrencyEnum.IDR.name -> CurrencyUtils.CurrencyEnum.IDR
    CurrencyUtils.CurrencyEnum.ILS.name -> CurrencyUtils.CurrencyEnum.ILS
    CurrencyUtils.CurrencyEnum.INR.name -> CurrencyUtils.CurrencyEnum.INR
    CurrencyUtils.CurrencyEnum.ISK.name -> CurrencyUtils.CurrencyEnum.ISK
    CurrencyUtils.CurrencyEnum.JPY.name -> CurrencyUtils.CurrencyEnum.JPY
    CurrencyUtils.CurrencyEnum.KRW.name -> CurrencyUtils.CurrencyEnum.KRW
    CurrencyUtils.CurrencyEnum.MXN.name -> CurrencyUtils.CurrencyEnum.MXN
    CurrencyUtils.CurrencyEnum.MYR.name -> CurrencyUtils.CurrencyEnum.MYR
    CurrencyUtils.CurrencyEnum.NOK.name -> CurrencyUtils.CurrencyEnum.NOK
    CurrencyUtils.CurrencyEnum.NZD.name -> CurrencyUtils.CurrencyEnum.NZD
    CurrencyUtils.CurrencyEnum.PHP.name -> CurrencyUtils.CurrencyEnum.PHP
    CurrencyUtils.CurrencyEnum.PLN.name -> CurrencyUtils.CurrencyEnum.PLN
    CurrencyUtils.CurrencyEnum.RON.name -> CurrencyUtils.CurrencyEnum.RON
    CurrencyUtils.CurrencyEnum.RUB.name -> CurrencyUtils.CurrencyEnum.RUB
    CurrencyUtils.CurrencyEnum.SEK.name -> CurrencyUtils.CurrencyEnum.SEK
    CurrencyUtils.CurrencyEnum.SGD.name -> CurrencyUtils.CurrencyEnum.SGD
    CurrencyUtils.CurrencyEnum.THB.name -> CurrencyUtils.CurrencyEnum.THB
    CurrencyUtils.CurrencyEnum.TRY.name -> CurrencyUtils.CurrencyEnum.TRY
    CurrencyUtils.CurrencyEnum.USD.name -> CurrencyUtils.CurrencyEnum.USD
    CurrencyUtils.CurrencyEnum.ZAR.name -> CurrencyUtils.CurrencyEnum.ZAR
    else -> CurrencyUtils.CurrencyEnum.XXX
}

fun String.toGeoPosition() = when (this) {
// Africa
    CurrencyUtils.CurrencyEnum.ZAR.name -> GeoPosition.AFRICA
// Asia
    CurrencyUtils.CurrencyEnum.CNY.name,
    CurrencyUtils.CurrencyEnum.HKD.name,
    CurrencyUtils.CurrencyEnum.IDR.name,
    CurrencyUtils.CurrencyEnum.ILS.name,
    CurrencyUtils.CurrencyEnum.INR.name,
    CurrencyUtils.CurrencyEnum.JPY.name,
    CurrencyUtils.CurrencyEnum.KRW.name,
    CurrencyUtils.CurrencyEnum.MYR.name,
    CurrencyUtils.CurrencyEnum.PHP.name,
    CurrencyUtils.CurrencyEnum.RUB.name,
    CurrencyUtils.CurrencyEnum.SGD.name,
    CurrencyUtils.CurrencyEnum.THB.name,
    CurrencyUtils.CurrencyEnum.TRY.name -> GeoPosition.ASIA
// Europe
    CurrencyUtils.CurrencyEnum.BGN.name,
    CurrencyUtils.CurrencyEnum.CHF.name,
    CurrencyUtils.CurrencyEnum.CZK.name,
    CurrencyUtils.CurrencyEnum.DKK.name,
    CurrencyUtils.CurrencyEnum.EUR.name,
    CurrencyUtils.CurrencyEnum.GBP.name,
    CurrencyUtils.CurrencyEnum.HRK.name,
    CurrencyUtils.CurrencyEnum.HUF.name,
    CurrencyUtils.CurrencyEnum.ISK.name,
    CurrencyUtils.CurrencyEnum.NOK.name,
    CurrencyUtils.CurrencyEnum.PLN.name,
    CurrencyUtils.CurrencyEnum.RON.name,
    CurrencyUtils.CurrencyEnum.SEK.name -> GeoPosition.EUROPE
// North America
    CurrencyUtils.CurrencyEnum.CAD.name,
    CurrencyUtils.CurrencyEnum.MXN.name,
    CurrencyUtils.CurrencyEnum.USD.name -> GeoPosition.NORTH_AMERICA
// Oceania
    CurrencyUtils.CurrencyEnum.AUD.name,
    CurrencyUtils.CurrencyEnum.NZD.name -> GeoPosition.OCEANIA
// South America
    CurrencyUtils.CurrencyEnum.BRL.name -> GeoPosition.SOUTH_AMERICA
// Fallback
    else -> GeoPosition.OTHER
}

