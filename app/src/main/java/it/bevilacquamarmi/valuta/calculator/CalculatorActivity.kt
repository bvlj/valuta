package it.bevilacquamarmi.valuta.calculator

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.KeyboardShortcutGroup
import android.view.KeyboardShortcutInfo
import android.view.Menu
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.robinhood.ticker.TickerUtils
import com.robinhood.ticker.TickerView
import it.bevilacquamarmi.valuta.BuildConfig
import it.bevilacquamarmi.valuta.R
import it.bevilacquamarmi.valuta.util.extensions.getSymbol

class CalculatorActivity: AppCompatActivity() {
    private lateinit var viewModel: CalculatorViewModel

    private lateinit var coordinator: CoordinatorLayout
    private lateinit var inputEditText: EditText
    private lateinit var outputTickerView: TickerView
    private lateinit var inputSpinner: Spinner
    private lateinit var outputSpinner: Spinner
    private lateinit var swapButton: FloatingActionButton

    private var toEurRatio = 1F
    private var toOutRatio = 1F

    private var extraName: String? = null

    private val editTextListener = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            val input = inputEditText.text.toString().replace(",", ".")

            outputTickerView.setText(String.format("%.2f",
                    if (input.isEmpty()) 0F
                    else calculate(input.toFloat())), true)
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_calculator)

        viewModel = ViewModelProviders.of(this)[CalculatorViewModel::class.java]

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.setNavigationIcon(R.drawable.ic_close_inverted)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        coordinator = findViewById(R.id.coordinator)
        inputEditText = findViewById(R.id.calculator_input)
        outputTickerView = findViewById(R.id.calculator_output)
        inputSpinner = findViewById(R.id.calculator_input_unit)
        outputSpinner = findViewById(R.id.calculator_output_unit)
        swapButton = findViewById(R.id.calculator_swap)

        val keyboardHint = findViewById<LinearLayout>(R.id.calculator_keyboard_hint)
        keyboardHint.visibility = if (hasHardwareKeyBoard()) View.VISIBLE else View.GONE

        extraName = intent.getStringExtra(EXTRA_NAME)

        outputTickerView.setCharacterList(TickerUtils.getDefaultNumberList())
        outputTickerView.setOnLongClickListener { copy(); true }
        swapButton.setOnClickListener { swap() }
        inputEditText.addTextChangedListener(editTextListener)

        setupSpinners()

        if (!resources.getBoolean(R.bool.isTablet)) {
            window.navigationBarColor = window.statusBarColor
        }
    }

    override fun onProvideKeyboardShortcuts(data: MutableList<KeyboardShortcutGroup>?,
                                            menu: Menu?, deviceId: Int) {
        super.onProvideKeyboardShortcuts(data, menu, deviceId)

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.N) {
            return
        }

        val shortcuts = ArrayList<KeyboardShortcutInfo>()
        // Shift left + C
        shortcuts.add(KeyboardShortcutInfo(
                getString(R.string.keyboard_copy),
                KeyEvent.KEYCODE_C,
                KeyEvent.KEYCODE_SHIFT_LEFT))
        // Shift right + C
        shortcuts.add(KeyboardShortcutInfo(
                getString(R.string.keyboard_copy),
                KeyEvent.KEYCODE_C,
                KeyEvent.KEYCODE_SHIFT_RIGHT))
        // Shift left + S
        shortcuts.add(KeyboardShortcutInfo(
                getString(R.string.keyboard_copy),
                KeyEvent.KEYCODE_S,
                KeyEvent.KEYCODE_SHIFT_LEFT))
        // Shift right + S
        shortcuts.add(KeyboardShortcutInfo(
                getString(R.string.keyboard_copy),
                KeyEvent.KEYCODE_S,
                KeyEvent.KEYCODE_SHIFT_RIGHT))

        data?.add(KeyboardShortcutGroup(getString(R.string.keyboard_app_title), shortcuts))
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?) =
        when (keyCode) {
            KeyEvent.KEYCODE_C -> {
                copy()
                true
            }
            KeyEvent.KEYCODE_S -> {
                swap()
                true
            }
            else -> super.onKeyDown(keyCode, event)
        }


    private fun setupSpinners() {
        inputSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                toEurRatio = viewModel.getRatioFor(p2)
                refresh()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) = Unit
        }

        outputSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                toOutRatio = viewModel.getRatioFor(p2)
                refresh()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) = Unit
        }

        val list = viewModel.list
        if (list.size < 2) {
            Toast.makeText(this, R.string.error_no_internet, Toast.LENGTH_LONG).show()
            finish()
            return
        }

        val array = Array(list.size) {""}

        for ((i, item) in list.withIndex()) {
            array[i] = item.name
        }

        val adapter = ArrayAdapter<String>(this,
                R.layout.support_simple_spinner_dropdown_item, array)

        /*
         * From USD
         * IN: USD ; OUT: EUR
         *
         * From EUR || ""
         * IN: EUR ; OUT: array[1]
         */
        val inPosition =
                if (extraName.isNullOrBlank()) array.indexOf(DEFAULT_CURRENCY)
                else array.indexOf(extraName)
        val outPosition =
                if (extraName.isNullOrBlank())
                    if (array[1] == DEFAULT_CURRENCY) 0 else 1
                else array.indexOf(DEFAULT_CURRENCY)

        inputSpinner.adapter = adapter
        outputSpinner.adapter = adapter

        inputSpinner.setSelection(inPosition)
        outputSpinner.setSelection(outPosition)
    }

    private fun swap() {
        val origPosition = inputSpinner.selectedItemPosition
        inputSpinner.setSelection(outputSpinner.selectedItemPosition, true)
        outputSpinner.setSelection(origPosition, true)

        val originalInput = inputEditText.text
        inputEditText.setText(outputTickerView.text)
        outputTickerView.setText(originalInput.toString(), true)

        swapButton.animate()
                .rotation(if (swapButton.rotation != 180f) 180f else 360f)
                .start()

        refresh()
    }

    private fun copy() {
        val manager =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) getSystemService(ClipboardManager::class.java)
            else getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        manager.primaryClip = ClipData.newPlainText("message", outputTickerView.text)

        Snackbar.make(coordinator, R.string.calculator_copied, Snackbar.LENGTH_SHORT)
                .setAction(R.string.currency_menu_share) { share() }
            .show()
    }

    private fun refresh() {
        // Pretty dumb way to trigger onTextChanged()
        inputEditText.text = inputEditText.text
        inputEditText.setSelection(inputEditText.text.length)
    }

    private fun hasHardwareKeyBoard() =
            when (resources.configuration.keyboard) {
                Configuration.KEYBOARD_QWERTY -> true
                else -> BuildConfig.DEBUG
            }

    fun calculate(input: Float) = input / toEurRatio * toOutRatio

    private fun share() {
        val inputCurrency = viewModel.list[inputSpinner.selectedItemPosition]
        val outputCurrency = viewModel.list[outputSpinner.selectedItemPosition]
        val content = getString(R.string.calculator_share_message,
                inputEditText.text,
                inputCurrency.getSymbol(resources),
                outputTickerView.text,
                outputCurrency.getSymbol(resources))

        val intent = Intent()
            .putExtra(Intent.EXTRA_TEXT, content)
            .setAction(Intent.ACTION_SEND)
            .setType("text/plain")

        startActivity(Intent.createChooser(intent, getString(R.string.share_with)))
    }

    companion object {
        const val EXTRA_NAME = "extraName"
        const val DEFAULT_CURRENCY = "EUR"
    }
}