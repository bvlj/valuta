package it.bevilacquamarmi.valuta.calculator

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import it.bevilacquamarmi.valuta.db.AppDatabase
import it.bevilacquamarmi.valuta.db.DatabaseTask
import it.bevilacquamarmi.valuta.db.entities.CurrencyInfo

class CalculatorViewModel(owner: Application): AndroidViewModel(owner) {
    var list: List<CurrencyInfo>

    private val db = AppDatabase.getInstance(owner)

    init {
        val task = FetchListTask(db)
        task.execute()
        list = task.get()
    }

    fun getRatioFor(position: Int) = list[position].ratio

    private class FetchListTask(db: AppDatabase): DatabaseTask<Unit, List<CurrencyInfo>>(db) {

        override fun doInBackground(vararg p0: Unit?): List<CurrencyInfo> {
            return db.currency().getAllStatic()
        }
    }
}