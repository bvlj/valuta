package it.bevilacquamarmi.valuta.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import it.bevilacquamarmi.valuta.db.AppDatabase
import it.bevilacquamarmi.valuta.db.DatabaseTask
import it.bevilacquamarmi.valuta.db.entities.CurrencyInfo
import it.bevilacquamarmi.valuta.util.CurrencyUtils
import it.bevilacquamarmi.valuta.util.GeoPosition
import it.bevilacquamarmi.valuta.util.extensions.toCurrencyEnum
import it.bevilacquamarmi.valuta.util.extensions.toGeoPosition
import it.bevilacquamarmi.valuta.util.network.FixerException
import it.bevilacquamarmi.valuta.util.network.LiveSyncStatus
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.StringBuilder
import java.net.HttpURLConnection
import java.net.URL

class MainActivityViewModel(owner: Application) : AndroidViewModel(owner) {
    val list: LiveData<List<CurrencyInfo>>
    val syncStatus: LiveSyncStatus

    private val db = AppDatabase.getInstance(owner)

    init {
        list = db.currency().getAll()
        syncStatus = LiveSyncStatus(owner)
    }

    fun updateDb(onPost: (String) -> Unit) {
        SyncTask(db, onPost).execute()
    }

    fun setFavorite(uid: Long, checked: Boolean) {
        SetFavoriteTask(db).execute(Pair(uid, checked))
    }

    fun generateShortcuts(onPost: (List<CurrencyInfo>) -> Unit) {
        GetFavoritesForShortcutsTask(db, onPost).execute()
    }

    fun getFilteredList(position: GeoPosition, onPost: (List<CurrencyInfo>) -> Unit) {
        GetFilteredTask(db, onPost).execute(position)
    }

    private class SyncTask(db: AppDatabase, private val onPost: (String) -> Unit) :
            DatabaseTask<Unit, String>(db) {
        private var favorites: List<CurrencyInfo>? = null

        override fun doInBackground(vararg p0: Unit): String {
            favorites = db.currency().getFavorites()

            try {
                val list = fetchFromWeb()
                db.currency().deleteAll()
                db.currency().insert(*list.toTypedArray())
            } catch (e: FixerException) {
                return e.message
            }

            return ""
        }

        override fun onPostExecute(result: String) {
            onPost(result)
        }

        private fun fetchRawJSON(): String {
            val url = URL(CurrencyUtils.FIXER_API_URL)
            val connection = url.openConnection() as HttpURLConnection
            connection.requestMethod = "GET"
            connection.doInput = true
            connection.connect()

            val reader = BufferedReader(InputStreamReader(connection.inputStream))
            var line: String? = ""
            val jsonBuilder = StringBuilder()
            while (line != null) {
                jsonBuilder.append(line)
                        .append('\n')
                line = reader.readLine()
            }
            reader.close()

            return jsonBuilder.toString()
        }

        /*
         * {
         *   "base": "***",
         *   "date": "YYYY-MM-DD",
         *   "rates": {
         *     "***": #.###,
         *     "***": #.###
         *   }
         * }
         */
        private fun fetchFromWeb(): List<CurrencyInfo> {
            val list = ArrayList<CurrencyInfo>()
            val json = JSONObject(fetchRawJSON().trim())

            val success = json.getBoolean("success")
            if (!success) {
                throw FixerException(json)
            }

            val rates = json.getJSONObject("rates")
            val iterator = rates.keys()

            list.add(CurrencyInfo(CurrencyUtils.DEFAULT_CURRENCY_UID, "EUR",
                    1F, isFavorite(CurrencyUtils.DEFAULT_CURRENCY_UID), "EUR".toGeoPosition()))

            while (iterator.hasNext()) {
                val name = iterator.next() as String
                val ratio = rates.get(name).toString().toFloat()
                val position = name.toGeoPosition()
                var uid: Int = name.toCurrencyEnum().ordinal
                if (uid == 0) {
                    uid = list.size
                }

                list.add(CurrencyInfo(uid.toLong(), name, ratio,
                        isFavorite(uid.toLong()), position))
            }

            return list
        }

        private fun isFavorite(uid: Long) = if (favorites?.any { it.uid == uid } == true) 1 else 0
    }

    private class SetFavoriteTask(db: AppDatabase) : DatabaseTask<Pair<Long, Boolean>, Unit>(db) {

        override fun doInBackground(vararg p0: Pair<Long, Boolean>?) {
            val arg = p0[0] ?: return

            val item = db.currency().getById(arg.first)[0]
            item.favorite = if (arg.second) 1 else 0

            db.currency().insert(item)
        }
    }

    private class GetFavoritesForShortcutsTask(db: AppDatabase,
                                               private val onPost: (List<CurrencyInfo>) -> Unit) :
            DatabaseTask<Unit, List<CurrencyInfo>>(db) {

        override fun doInBackground(vararg p0: Unit?): List<CurrencyInfo> =
                db.currency().getFavorites()
                        .filter { it -> it.name != "EUR" } // No need to add eur
                        .subList(4)

        override fun onPostExecute(result: List<CurrencyInfo>?) {
            result?.run { onPost(this) }
        }

        fun List<CurrencyInfo>.subList(upTo: Int): List<CurrencyInfo> {
            return when {
                size == 0 -> emptyList()
                size <= upTo -> this
                else -> subList(0, upTo)
            }
        }
    }

    private class GetFilteredTask(db: AppDatabase,
                                  private val onPost: (List<CurrencyInfo>) -> Unit) :
            DatabaseTask<GeoPosition, List<CurrencyInfo>>(db) {

        override fun doInBackground(vararg position: GeoPosition): List<CurrencyInfo> {
            val argument = position[0].ordinal

            return if (argument == GeoPosition.OTHER.ordinal) {
                db.currency().getAllStatic()
            } else {
                db.currency().getByGeoPosition(argument)
            }
        }

        override fun onPostExecute(result: List<CurrencyInfo>?) {
            result?.run { onPost(this) }
        }
    }
}
