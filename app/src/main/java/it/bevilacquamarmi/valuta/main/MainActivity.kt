package it.bevilacquamarmi.valuta.main

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.drawable.Icon
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.RelativeLayout
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.chip.ChipGroup
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import it.bevilacquamarmi.valuta.R
import it.bevilacquamarmi.valuta.calculator.CalculatorActivity
import it.bevilacquamarmi.valuta.db.entities.CurrencyInfo
import it.bevilacquamarmi.valuta.detail.DetailsActivity
import it.bevilacquamarmi.valuta.ui.CurrencyAdapter
import it.bevilacquamarmi.valuta.ui.CurrencyItemCallbacks
import it.bevilacquamarmi.valuta.ui.RecyclerViewExt
import it.bevilacquamarmi.valuta.util.GeoPosition
import it.bevilacquamarmi.valuta.util.extensions.getFlag
import it.bevilacquamarmi.valuta.util.extensions.getSymbol
import it.bevilacquamarmi.valuta.util.extensions.set
import it.bevilacquamarmi.valuta.util.network.LiveSyncStatus

class MainActivity : AppCompatActivity(), CurrencyItemCallbacks {
    private lateinit var coordinator: CoordinatorLayout
    private lateinit var filterLayout: RelativeLayout
    private lateinit var filterGroup: ChipGroup
    private lateinit var refreshLayout: SwipeRefreshLayout
    private lateinit var fab: FloatingActionButton

    private lateinit var viewModel: MainActivityViewModel
    private lateinit var adapter: CurrencyAdapter
    private lateinit var broadcastManager: LocalBroadcastManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this)[MainActivityViewModel::class.java]
        broadcastManager = LocalBroadcastManager.getInstance(this)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        coordinator = findViewById(R.id.coordinator)
        fab = findViewById(R.id.fab)
        filterLayout = findViewById(R.id.main_filter)
        filterGroup = findViewById(R.id.filter_chip_group)
        refreshLayout = findViewById(R.id.main_refreshlayout)

        fab.setOnClickListener { openCalculator(null) }
        refreshLayout.setOnRefreshListener { refresh() }
        refreshLayout.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorAccent))
        filterGroup.setOnCheckedChangeListener { _, i -> onFilterSelected(i) }

        val recyclerView = findViewById<RecyclerViewExt>(R.id.main_recyclerview)
        adapter = CurrencyAdapter(this, this)
        recyclerView.adapter = adapter

        viewModel.list.observe(this, Observer { it ->
            adapter.list = it ?: arrayListOf()
        })

        viewModel.syncStatus.observe(this, Observer(this::promptSyncStatus))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menu?.let { menuInflater.inflate(R.menu.menu_main, it) }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (R.id.menu_main_filter == item?.itemId) {
            filterLayout.visibility =
                if (filterLayout.visibility == View.VISIBLE) View.GONE
                else View.VISIBLE
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showDetails(currencyInfo: CurrencyInfo) {
        val intent = Intent(this, DetailsActivity::class.java)
            .putExtra(DetailsActivity.EXTRA_NAME, currencyInfo.name)
        startActivity(intent)
    }

    override  fun openCalculator(currencyInfo: CurrencyInfo?) {
        val intent = Intent(this, CalculatorActivity::class.java)
        if (currencyInfo != null) {
            intent.putExtra(CalculatorActivity.EXTRA_NAME, currencyInfo.name)
        }

        val options = ActivityOptionsCompat
            .makeSceneTransitionAnimation(this, fab, fab.transitionName)
        startActivity(intent, options.toBundle())
    }

    override  fun copyCurrency(currencyInfo: CurrencyInfo) {
        val clipboard = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            getSystemService(ClipboardManager::class.java)
        else
            getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager

        clipboard.primaryClip = ClipData.newPlainText("message", currencyInfo.getCopyText())

        Snackbar.make(coordinator, R.string.calculator_copied, Snackbar.LENGTH_LONG)
            .show()
    }

    override  fun shareCurrency(currencyInfo: CurrencyInfo) {
        val message = getString(R.string.share_message, currencyInfo.ratio, currencyInfo.getSymbol(resources))
        val intent = Intent()
            .putExtra(Intent.EXTRA_TEXT, message)
            .setAction(Intent.ACTION_SEND)
            .setType("text/plain")

        startActivity(Intent.createChooser(intent, getString(R.string.share_with)))
    }

    override fun setFavoriteCurrency(currencyInfo: CurrencyInfo, checked: Boolean) {
        viewModel.setFavorite(currencyInfo.uid, checked)
    }

    @RequiresApi(25)
    private fun updateShortcuts(list: List<CurrencyInfo>) {
        val manager = getSystemService(Context.SHORTCUT_SERVICE) as ShortcutManager
        manager.removeAllDynamicShortcuts()

        val shortcuts = ArrayList<ShortcutInfo>()
        for (item in list) {
            val intent = Intent(this, DetailsActivity::class.java)
            intent.action = Intent.ACTION_VIEW
            intent.putExtra(DetailsActivity.EXTRA_NAME, item.name)

            shortcuts.add(ShortcutInfo.Builder(this, item.name)
                    .setShortLabel(item.name)
                    .setLongLabel(item.name)
                    .setIntent(intent)
                    .setIcon(Icon.createWithResource(this, item.getFlag()))
                    .build())
        }

        manager.dynamicShortcuts = shortcuts
    }

    private fun refresh() {
        val noInternet = checkInternet()

        if (noInternet) {
            broadcastManager.sendBroadcast(Intent(LiveSyncStatus.BROADCAST_ACTION_SYNC))
            return
        }

        viewModel.updateDb {
            if (it.isNotEmpty()) {
                showError(it)
            } else {
                onUpdateCompleted()
            }
        }
    }

    private fun checkInternet(): Boolean {
        if (viewModel.syncStatus.value == LiveSyncStatus.NO_CONNECTION) {
            promptSyncStatus(LiveSyncStatus.NO_CONNECTION)

            refreshLayout.isRefreshing = false
            return true
        }

        return false
    }

    private fun promptSyncStatus(@LiveSyncStatus.Companion.SyncStatus status: Int?) {
        if (status == null || status == LiveSyncStatus.UP_TO_DATE) {
            return
        }


        val message = if (status == LiveSyncStatus.NO_CONNECTION)
            R.string.error_no_internet
        else
            R.string.sync_status_prompt
        val action = if (status == LiveSyncStatus.NO_CONNECTION)
            R.string.retry
        else
            R.string.sync_status_action

        Snackbar.make(coordinator, message, Snackbar.LENGTH_LONG)
                .setAction(action) { refresh() }
            .show()
    }

    private fun showError(message: String) {
        Snackbar.make(coordinator, message, Snackbar.LENGTH_LONG)
            .setAction(R.string.retry) { refresh() }
            .show()
    }

    private fun onUpdateCompleted() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        prefs[LiveSyncStatus.KEY_LAST_SYNC] = System.currentTimeMillis()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            viewModel.generateShortcuts { list -> updateShortcuts(list) }
        }

        Handler().postDelayed({
            broadcastManager.sendBroadcast(Intent(LiveSyncStatus.BROADCAST_ACTION_SYNC))
            refreshLayout.isRefreshing = false
        }, 1000L)
    }

    private fun onFilterSelected(position: Int) {
        filterGroup.check(position)

        val geo = when (filterGroup.checkedChipId) {
            R.id.dialog_filter_chip_africa -> GeoPosition.AFRICA
            R.id.dialog_filter_chip_asia -> GeoPosition.ASIA
            R.id.dialog_filter_chip_europe -> GeoPosition.EUROPE
            R.id.dialog_filter_chip_north_america -> GeoPosition.NORTH_AMERICA
            R.id.dialog_filter_chip_oceania -> GeoPosition.OCEANIA
            R.id.dialog_filter_chip_south_america -> GeoPosition.SOUTH_AMERICA
            else -> GeoPosition.OTHER
        }

        viewModel.getFilteredList(geo) { it -> adapter.list = it }
    }
}
