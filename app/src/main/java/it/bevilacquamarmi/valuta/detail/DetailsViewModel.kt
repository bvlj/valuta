package it.bevilacquamarmi.valuta.detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import android.os.AsyncTask
import it.bevilacquamarmi.valuta.util.CurrencyUtils
import it.bevilacquamarmi.valuta.util.SystemUtils
import it.bevilacquamarmi.valuta.util.network.LiveSyncStatus
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.StringBuilder
import java.net.HttpURLConnection
import java.net.URL

class DetailsViewModel(owner: Application): AndroidViewModel(owner) {
    val syncStatus = LiveSyncStatus(owner)
    var name = ""

    // Return true if there was an error
    fun fetchData(onPost:(Array<Float>) -> Unit): Boolean {
        if (syncStatus.value == LiveSyncStatus.NO_CONNECTION) {
            return true
        }

        val address = CurrencyUtils.getHistoryAPIURL("%1\$s", name)
        val task = FetchDataTask(address)

        return try {
            task.execute(name)
            val data = task.get()
            onPost(data)
            false
        } catch (e: Exception) {
            true
        }
    }

    private class FetchDataTask(private val address: String) :
            AsyncTask<String, Unit, Array<Float>>() {

        override fun doInBackground(vararg p0: String?): Array<Float> {
            val name = p0[0] ?: ""
            if (name.isBlank()) {
                return arrayOf()
            }

            if (name == "EUR") {
                return Array(CONFIG_FETCH_DAYS) { 1F }
            }

            val dates = Array(CONFIG_FETCH_DAYS) { "1999-01-01" }
            for (i in CONFIG_FETCH_DAYS downTo 1) {
                dates[CONFIG_FETCH_DAYS - i] = SystemUtils.getDate(-3 * i + 1)
            }

            val results = Array(CONFIG_FETCH_DAYS) { 0F }

            for (i in 0..(CONFIG_FETCH_DAYS - 1)) {
                results[i] = fetchFromWeb(address.format(dates[i]))
            }

            return results
        }


        private fun fetchRawJSON(url: String): String {
            val connection = URL(url).openConnection() as HttpURLConnection
            connection.requestMethod = "GET"
            connection.doInput = true
            connection.connect()

            val reader = BufferedReader(InputStreamReader(connection.inputStream))
            var line: String? = ""
            val jsonBuilder = StringBuilder()
            while (line != null) {
                jsonBuilder.append(line)
                        .append('\n')
                line = reader.readLine()
            }
            reader.close()

            return jsonBuilder.toString()
        }

        /*
         * {
         *   "base": "***",
         *   "date": "YYYY-MM-DD",
         *   "rates": {
         *     "***": #.###,
         *   }
         * }
         */
        private fun fetchFromWeb(url: String): Float {
            var ratio = 0F
            val json = JSONObject(fetchRawJSON(url).trim())

            val rates = json.getJSONObject("rates")
            val iterator = rates.keys()

            while (iterator.hasNext()) {
                ratio = rates.get(iterator.next()).toString().toFloat()
            }

            return ratio
        }
    }

    companion object {
        const val CONFIG_FETCH_DAYS = 5
    }
}