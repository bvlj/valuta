package it.bevilacquamarmi.valuta.detail

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.text.HtmlCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.robinhood.spark.SparkView
import it.bevilacquamarmi.valuta.R
import it.bevilacquamarmi.valuta.calculator.CalculatorActivity
import it.bevilacquamarmi.valuta.ui.DetailsGraphAdapter
import it.bevilacquamarmi.valuta.util.CurrencyUtils
import it.bevilacquamarmi.valuta.util.network.LiveSyncStatus

class DetailsActivity: AppCompatActivity() {
    private lateinit var viewModel: DetailsViewModel

    private lateinit var loadLayout: RelativeLayout
    private lateinit var loadText: TextView
    private lateinit var loadBar: ProgressBar
    private lateinit var loadButton: Button

    private lateinit var dataLayout: LinearLayout
    private lateinit var dataGraph: SparkView
    private lateinit var dataSummary: TextView
    private lateinit var dataButton: Button

    private lateinit var name: String

    private var hasDetectedStatus = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_details)

        name = intent.getStringExtra(EXTRA_NAME)
        viewModel = ViewModelProviders.of(this)[DetailsViewModel::class.java]
        viewModel.name = name

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = name
        setSupportActionBar(toolbar)
        toolbar.setNavigationIcon(R.drawable.ic_close)
        toolbar.setNavigationOnClickListener { finish() }

        loadLayout = findViewById(R.id.details_load_layout)
        loadText = findViewById(R.id.details_load_text)
        loadBar = findViewById(R.id.details_load_bar)
        loadButton = findViewById(R.id.details_load_button)

        dataLayout = findViewById(R.id.details_data_layout)
        dataGraph = findViewById(R.id.details_data_graph)
        dataSummary = findViewById(R.id.details_data_summary)
        dataButton = findViewById(R.id.details_data_button)

        loadButton.setOnClickListener { load() }
        dataButton.setOnClickListener { openCalculator(name) }

        viewModel.syncStatus.observe(this, Observer {
            if (!hasDetectedStatus) {
                load()
                hasDetectedStatus = true }
            })

        showLoadUI()
    }

    private fun load() {
        if ("EUR" == viewModel.name) {
            showUI(arrayOf(1f, 1f))
            return
        }

        if (viewModel.syncStatus.value == LiveSyncStatus.NO_CONNECTION) {
            Handler().postDelayed({ showNoInternetUI() }, 1000)
            return
        }

        showLoadUI()

        // Load data in the background
        try {
            Thread {
                if (viewModel.fetchData { data -> runOnUiThread { showUI(data) } }) {
                    runOnUiThread { showNoInternetUI() }
                }
            }.start()
        } catch (e: Exception) {
            showNoInternetUI()
        }
    }

    private fun showLoadUI() {
        loadText.text = getString(R.string.loading)
        loadButton.visibility = View.GONE

        loadBar.visibility = View.VISIBLE
    }

    private fun showNoInternetUI() {
        loadBar.visibility = View.GONE

        loadText.text = getString(R.string.error_no_internet)
        loadButton.visibility = View.VISIBLE
    }

    private fun showUI(array: Array<Float>) {
        loadBar.visibility = View.GONE
        loadLayout.visibility = View.GONE
        dataLayout.visibility = View.VISIBLE
        dataGraph.adapter = DetailsGraphAdapter(array)

        val symbol = CurrencyUtils.getSymbol(resources, name)
        val description = getString(R.string.details_summary,
                symbol, array.last(), array.max(), array.min(), array.average())
        dataSummary.text = HtmlCompat.fromHtml(description, HtmlCompat.FROM_HTML_MODE_COMPACT)
    }

    private fun openCalculator(name: String) {
        val intent = Intent(this, CalculatorActivity::class.java)
            .putExtra(CalculatorActivity.EXTRA_NAME, name)
        startActivity(intent)
    }

    companion object {
        const val EXTRA_NAME = "extraName"
    }
}