package it.bevilacquamarmi.valuta.ui

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.bevilacquamarmi.valuta.R

class RecyclerViewExt(context: Context, attrs: AttributeSet) : RecyclerView(context, attrs) {

    init {
        val isTablet = context.resources.getBoolean(R.bool.isTablet)
        itemAnimator = DefaultItemAnimator()
        layoutManager = if (isTablet) GridLayoutManager(context, 2) else LinearLayoutManager(context)
    }
}
