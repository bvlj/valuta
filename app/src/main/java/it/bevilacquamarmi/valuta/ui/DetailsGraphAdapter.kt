package it.bevilacquamarmi.valuta.ui

import com.robinhood.spark.SparkAdapter

class DetailsGraphAdapter(private val data: Array<Float>) : SparkAdapter() {

    override fun getCount() = data.size

    override fun getItem(index: Int) = data[index]

    override fun getY(index: Int) = data[index]
}