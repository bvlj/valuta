package it.bevilacquamarmi.valuta.ui

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.view.menu.MenuPopupHelper
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import it.bevilacquamarmi.valuta.R
import it.bevilacquamarmi.valuta.db.entities.CurrencyInfo
import it.bevilacquamarmi.valuta.util.extensions.getFlag

class CurrencyAdapter(
    private val context: Context,
    private val callbacks: CurrencyItemCallbacks) : RecyclerView.Adapter<CurrencyAdapter.CurrencyHolder>() {

    var list: List<CurrencyInfo> = emptyList()
        set(value) {
            val diff = DiffUtil.calculateDiff(CurrencyDiff(value))
            field = value
            diff.dispatchUpdatesTo(this)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyHolder =
            CurrencyHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_currency, parent, false))

    override fun onBindViewHolder(holder: CurrencyHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int = list.size

    inner class CurrencyHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private val icon = view.findViewById<ImageView>(R.id.item_currency_flag)
        private val name = view.findViewById<TextView>(R.id.item_currency_name)
        private val ratio = view.findViewById<TextView>(R.id.item_currency_ratio)
        private val favorite = view.findViewById<ImageView>(R.id.item_currency_favorite)

        private lateinit var currency: CurrencyInfo

        fun bind(item: CurrencyInfo) {
            currency = item

            name.text = currency.name
            ratio.text = String.format("%.2f", currency.ratio)
            icon.setImageResource(currency.getFlag())

            var isChecked = currency.favorite == 1
            setChecked(isChecked)

            favorite.setOnClickListener { _ ->
                isChecked = !isChecked
                callbacks.setFavoriteCurrency(currency, isChecked)
                setChecked(isChecked)
            }

            view.setOnClickListener { callbacks.showDetails(currency) }
            view.setOnLongClickListener { showContextMenu() }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                view.setOnContextClickListener { showContextMenu() }
            }
        }

        private fun setChecked(isChecked: Boolean) {
            favorite.setImageResource(
                    if (isChecked) R.drawable.ic_favorite_full
                    else R.drawable.ic_favorite_empty)
            favorite.contentDescription = context.getString(
                    if (isChecked) R.string.currency_icon_favorite_added
                    else R.string.currency_icon_favorite_add)
            view.setBackgroundColor(ContextCompat.getColor(context,
                    if (isChecked) R.color.favorite_bg else android.R.color.transparent))
        }

        @SuppressLint("RestrictedApi")
        private fun showContextMenu(): Boolean {
            val anchor = name
            val wrapper = ContextThemeWrapper(context, R.style.AppTheme_PopupMenuOverlapAnchor)
            val menu = buildMenu(anchor, wrapper)
            val helper = MenuPopupHelper(wrapper, menu.menu as MenuBuilder, anchor)
            helper.setForceShowIcon(true)
            helper.show()

            return true
        }

        private fun buildMenu(view: View, wrapper: ContextThemeWrapper): PopupMenu {
            val menu = PopupMenu(wrapper, view, Gravity.NO_GRAVITY,
                    R.attr.actionOverflowMenuStyle, 0)
            menu.inflate(R.menu.menu_item_currency)

            menu.setOnMenuItemClickListener { menuItem ->
                when(menuItem.itemId) {
                    R.id.menu_currency_open -> { callbacks.showDetails(currency) }
                    R.id.menu_currency_convert -> { callbacks.openCalculator(currency) }
                    R.id.menu_currency_copy -> { callbacks.copyCurrency(currency) }
                    R.id.menu_currency_share -> { callbacks.shareCurrency(currency) }
                }
                true
            }

            return menu
        }
    }

    private inner class CurrencyDiff(private val newList: List<CurrencyInfo>): DiffUtil.Callback() {

        override fun getOldListSize() = list.size
        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                list[oldItemPosition].uid == newList[newItemPosition].uid

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                list[oldItemPosition] == newList[newItemPosition]
    }
}
