package it.bevilacquamarmi.valuta.ui

import it.bevilacquamarmi.valuta.db.entities.CurrencyInfo

interface CurrencyItemCallbacks {

    fun showDetails(currencyInfo: CurrencyInfo)
    fun openCalculator(currencyInfo: CurrencyInfo?)
    fun copyCurrency(currencyInfo: CurrencyInfo)
    fun shareCurrency(currencyInfo: CurrencyInfo)
    fun setFavoriteCurrency(currencyInfo: CurrencyInfo, checked: Boolean)
}